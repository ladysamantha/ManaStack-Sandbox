let gulp = require('gulp');
let gulpif = require('gulp-if');
let babel = require('gulp-babel');
let browserify = require('gulp-browserify');
let less = require('gulp-less');
let cssmin = require('gulp-cssmin');
let htmlmin = require('gulp-htmlmin');
let uglify = require('gulp-uglify');
let argv = require('yargs').argv;

gulp.task('watch', (done) => {
	gulp.watch('./js/**/*.js', gulp.series('transpile', 'browserify'));
	gulp.watch('./css/**/*.less', gulp.series('less'));
});

gulp.task('transpile', (done) => {
	return gulp.src('./js/**/*.js')
		.pipe(babel({
			compact: false,
			presets: ['react', 'es2015']
		}))
		.on('error', (err) => {
			console.log(err.message);
			done();
		})
		.pipe(gulp.dest('./build'));
});

gulp.task('browserify', (done) => {
	return gulp.src('./build/app.js')
		.pipe(browserify({
			insertGlobals: true,
			extensions: ['.jsx', '.js'],
		}))
		.on('error', (err) => {
			console.log(err.message);
			done();
		})
		.pipe(gulpif(argv.production, uglify()))
		.pipe(gulp.dest('./build'));
});

gulp.task('less', (done) => {
	return gulp.src('./css/style.less')
		.pipe(less())
		.on('error', (err) => {
			console.log(err.message);
			done();
		})
		.pipe(gulpif(argv.production, cssmin()))
		.pipe(gulp.dest('./css'));
});


gulp.task('build', gulp.series('transpile', 'browserify', 'less'));
