# ManaStack-Sandbox

Deck playtesting component for ManaStack decks.

ManaStack.com is based on Angular 1.x, so there is a couple less-than-perfect design decisions in here to ensure that this react component interoperates properly with the angular codebase. However it functions perfectly fine as a standalone component.

A `gulp build` will build the application.

Pull requests are welcome!
