
let React = require('react');

let DragDrop = require('./../services/DragDrop');
let CardContextMenu = require('./CardContextMenu');

class Card extends React.Component {
	constructor(params) {
		super(params);
		this.state = {
			card: this.props.card,
			tapped: false,
			dragging: false,
			dragX: 0,
			dragY: 0,
			contextMenu: false,
		};
		this.backImageSrc = "https://manastack.com/images/cardback.jpg";
	}
	componentDidMount() {

	}
	componentWillReceiveProps(nextProps) {
		this.setState({
			card: nextProps.card,
		});
	}
	getImageSrc() {
		if (this.props.zone.props.name === "library") return "https://manastack.com/img/cardback.png";
		return "https://manastack.com/images/" + this.state.card.set.slug + "/" + this.state.card.image + ".jpg";
	}
	onDragStart(e) {
		var ev = e.nativeEvent;
		//Firefox requires this data to be set, or other drag events wont fire.
		//Because we can't prevent users from dragging to say, another application input field, set it to something sane
		ev.dataTransfer.setData('text/plain', this.state.card.name);
		DragDrop.setDragData(this.state.card);
		ev.dropEffect = "move";
		ev.dataTransfer.setDragImage(new Image(),0,0);
		this.setState({
			dragging:true,
		});
	}
	onDrag(e) {
		var ev = e.nativeEvent;
		var x = ev.clientX;
		var y = ev.clientY;
		DragDrop.setPreviewImage(this.getImageSrc());
		DragDrop.showPreview();
		DragDrop.setDragItem(this);
		DragDrop.setDragOrigin(this.props.zone);
	}
	componentWillUmount() {

	}
	onDragEnd(e) {
		e.preventDefault();
		this.setState({
			dragging:false,
		})
		DragDrop.hidePreview()
	}
	tap() {
		var zone = this.props.zone.props.name;
		if (zone === "battlefield" || zone === "lands") {
			this.props.zone.updateCard(Object.assign(this.state.card, { tapped: !this.state.card.tapped }));
		}
	}
	isTapped() {
		var zone = this.props.zone.props.name;
		if (zone === "battlefield" || zone === "lands") {
			return this.state.card.tapped;
		}
		else {
			return false;
		}
	}
	onDoubleClick(e) {
		e.preventDefault();
		this.tap();
	}
	onRightClick(e) {
		e.preventDefault();
		var ev = e.nativeEvent;
		var x = ev.clientX;
		var y = ev.clientY;
		this.setState({
			contextMenu: true,
			contextMenuX: x,
			contextMenuY: y,
		})
	}
	moveToLibrary(front) {
		this.props.zone.moveCardToZone(this.state.card, 'library', front);
	}
	closeContextMenu() {
		this.setState({
			contextMenu: false,
		});
	}
	addCounter(value) {
		var card = this.state.card;
		if (card.counter === undefined) card.counter = 0;
		this.props.zone.updateCard(Object.assign(card, {
			counter: card.counter += value
		}))
	}
	resetCounter() {
		this.props.zone.updateCard(Object.assign(this.state.card, {
			counter: 0,
		}))
	}
	render() {
		var card = this.props.card;
		if (!card) {
			return <div> </div>;
		}
		return <div className={"card-container " + ((this.isTapped()) ? "tapped" : "")} style={this.props.style}
			draggable={true}
			onDragStart={(e) => this.onDragStart(e)}
			onDrag={(e) => this.onDrag(e)}
			onDragEnd={(e) => this.onDragEnd(e)}
			onDoubleClick={(e) => this.onDoubleClick(e)}
			onContextMenu={(e) => this.onRightClick(e)}>
			<div className="card-image-container">
			<img style={{opacity: ((this.state.dragging) ? 0 : 1)}} src={this.getImageSrc()}/>
			{(() => {
				if (this.props.zone.props.name !== 'library') {
					return <div>
					{(() => {
						if (this.state.card.counter) return <div className="counter">{((this.state.card.counter > 0) ? "+" : "") + this.state.card.counter}</div>
					})()}
					{(() => {
						if (this.state.contextMenu) return <CardContextMenu card={this} x={this.state.contextMenuX} y={this.state.contextMenuY}/>
					})()}
					</div>
				}
			})()}
			</div>
		</div>;
	}
}

module.exports = Card;
