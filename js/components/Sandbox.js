
let React = require('react/');

let Zone = require('./Zone');
let GraveyardZone = require('./GraveyardZone');
let ExileZone = require('./ExileZone');
let Counter = require('./Counter');
let CardList = require('./../services/CardList');

class Sandbox extends React.Component {
	constructor(params) {
		super(params);
		this.state = {
			deck: this.props.deck,
			cards: [],
			turnNumber: 1,
			mulligans: 0,
			userLifeTotal: 20,
			enemyLifeTotal: 20,
			library: new CardList(),
			hand: new CardList(),
			lands: new CardList(),
			exile: new CardList(),
			graveyard: new CardList(),
			battlefield: new CardList(),
			mainboard: new CardList(),
			sideboard: new CardList(),
		};
		this.counterRefs = [];
	}
	componentDidMount() {
		this.reset();
	}
	drawCard() {
		var library = this.state.library;
		var card = this.state.library.slice(-1, 0).get(0);
		library = library.removeCard(card);
		this.setState({
			library: library,
			hand: this.state.hand.addCardToBack(card),
		})
	}
	drawHand() {
		var numCards = 7 - this.state.mulligans;
		for (var i=0;i<numCards;i++) {
			this.drawCard();
		}
	}
	shuffleLibrary() {
		this.setState({
			library: this.state.library.shuffle(),
		})
	}
	mulligan() {
		this.setState({
			mulligans: this.state.mulligans + 1,
		});
		this.reset();
	}
	reset(mulligans) {
		var mainboard = [];
		var sideboard = [];
		var deck = JSON.parse(JSON.stringify(this.state.deck)); //Fresh copy of the deck each reset
		deck.cards.forEach((card) => {
			if (card.sideboard) {
				sideboard.push(card.card);
			} else {
				mainboard.push(card.card);
			}
		});
		this.setState({
			mainboard: new CardList(mainboard),
			sideboard: new CardList(sideboard),
			library: new CardList(mainboard).shuffle(),
			hand: new CardList(),
			lands: new CardList(),
			exile: new CardList(),
			graveyard: new CardList(),
			battlefield: new CardList(),
			userLifeTotal: 20,
			enemyLifeTotal: 20,
			turnNumber: 1,
		});
		if (mulligans) {
			this.setState({
				mulligans: 0,
			})
		}
		setTimeout(() => {
			this.drawHand(); //Initial load
		}, 150);
		this.counterRefs.forEach((ref) => {
			if (ref) ref.reset();
		})
	}
	nextTurn() {
		this.setState({
			turnNumber: this.state.turnNumber + 1,
		});
		this.drawCard();
		this.state.lands.cards.forEach((card) => {
			card.tapped = false;
			this.setState({
				lands: this.state.lands.updateCard(card),
			});
		})
		this.state.battlefield.cards.forEach((card) => {
			card.tapped = false;
			this.setState({
				battlefield: this.state.battlefield.updateCard(card),
			});
		})
	}
	moveCard(card, zone1, zone2, front) {
		var update = {};
		update[zone1] = this.state[zone1].removeCard(card);
		if (front) {
			update[zone2] = this.state[zone2].addCardToFront(card);
		} else {
			update[zone2] = this.state[zone2].addCardToBack(card);
		}

		this.setState(update)
	}
	fetchLibraryCard(e) {
		var index = e.nativeEvent.target.value;
		if (index !== -1) {
			var card = this.state.library.slice(index,1).get(0);
			this.setState({
				hand: this.state.hand.addCardToBack(card),
				library: this.state.library.removeCard(card),
			});
			e.nativeEvent.target.value = -1;
		}
	}
	updateCard(oldCard, newCard) {
		this.state.deck.updateCard(oldCard, newCard);
	}
	render() {
		return <div className="manastack-sandbox">
			<div className="sandbox-playarea">
				<Zone parent={this} cards={this.state.battlefield} name="battlefield" size={80}/>
				<ExileZone parent={this} cards={this.state.exile} name="exile" size={20}/>
				<Zone parent={this} cards={this.state.lands} name="lands" size={80}/>
				<GraveyardZone parent={this} cards={this.state.graveyard} name="graveyard" size={20}/>
				<Zone parent={this} cards={this.state.hand} name="hand" size={80}/>
				<Zone parent={this} cards={this.state.library} name="library" size={20}/>
			</div>
			<div className="row sandbox-actions">
				<div className="col-sm-3">
					<span className="action-label">Turn {this.state.turnNumber}</span>
					<span className="action-label">Your Life</span>
					<div className="counter-container">
					<button className="btn btn-primary" onClick={() => this.setState({userLifeTotal:this.state.userLifeTotal-1})}>-</button>
					{this.state.userLifeTotal}
					<button className="btn btn-primary" onClick={() => this.setState({userLifeTotal:this.state.userLifeTotal+1})}>+</button>
					</div>
					<div className="counter-container">
					<span className="action-label">Enemy Life</span>
					<button className="btn btn-primary" onClick={() => this.setState({enemyLifeTotal:this.state.enemyLifeTotal-1})}>-</button>
					{this.state.enemyLifeTotal}
					<button className="btn btn-primary" onClick={() => this.setState({enemyLifeTotal:this.state.enemyLifeTotal+1})}>+</button>
					</div>
				</div>
				<div className="col-sm-3">
					<span className="action-label">Counters</span>
					<Counter ref={(ref) => this.counterRefs.push(ref)} initialValue="0"></Counter>
					<Counter ref={(ref) => this.counterRefs.push(ref)} initialValue="0"></Counter>
					<Counter ref={(ref) => this.counterRefs.push(ref)} initialValue="0"></Counter>
				</div>
				<div className="col-sm-3">
					<span className="action-label">Actions</span>
					<button className="btn btn-primary" onClick={() => this.drawCard()}>Draw Card</button><br/>
					<button className="btn btn-primary" onClick={() => this.nextTurn()}>Next Turn</button><br/>
					<button className="btn btn-primary" onClick={() => this.shuffleLibrary()}>Shuffle Library</button>
				</div>
				<div className="col-sm-3">
					<br/>
					<select className="form-control" onChange={(e) => this.fetchLibraryCard(e)}>
					<option value="-1">Search Library</option>
					{this.state.library.toJS().reverse().map((card, i) => {
						return <option value={this.state.library.length() - i - 1} key={i}>{card.name} ({i+1})</option>
					})}
					</select>
					<br/>

					<br/>
					<button className="btn btn-primary" onClick={() => this.mulligan()}>Mulligan</button>
					<button className="btn btn-primary" onClick={() => this.reset(true)}>Reset</button>
				</div>
			</div>
			<div className="clearfix"></div>
		</div>;
	}
}


module.exports = Sandbox;
