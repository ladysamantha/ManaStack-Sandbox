
let React = require('react');

class Counter extends React.Component {
	constructor(params) {
		super(params);
		this.state = {
			counter: parseInt(this.props.initialValue),
		};
	}
	componentDidMount() {
		
	}
	componentWillReceiveProps(nextProps) {

	}
	reset() {
		this.setState({
			counter: parseInt(this.props.initialValue),
		});
	}
	render() { 
		return <div className="counter-container">
			<button className="btn btn-primary" onClick={() => this.setState({counter:(this.state.counter - 1)})}>-</button>
			{this.state.counter}
			<button className="btn btn-primary" onClick={() => this.setState({counter:(this.state.counter + 1)})}>+</button>
		</div>;
	}
}

module.exports = Counter;