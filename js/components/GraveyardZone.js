
let React = require('react');
let Card = require('./Card');
let CardPile = require('./CardPile');
let Zone = require('./Zone');
let DragDrop = require('./../services/DragDrop');

class GraveyardZone extends Zone {
	constructor(params) {
		super(params);
		this.state = {
			cards: this.props.cards,
		};
		this.parent = this.props.parent;
	}
	render() {
		return <div className={"zone zone-" + this.props.name} style={{width:this.props.size + "%"}}
			onDrop={(e) => this.onDrop(e)} onDragOver={(e) => this.onDragOver(e)}>
			<div className="zone-inner">
			<span className="zone-label">{this.props.name}</span>
				<CardPile cards={this.state.cards} zone={this}/>
			</div>
		</div>
	}
}

module.exports = GraveyardZone;
