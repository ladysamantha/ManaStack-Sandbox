
let React = require('react');
let ReactDOM = require('react-dom');
let DragDrop = require('./../services/DragDrop');

class CardContextMenu extends React.Component {
	constructor(params) {
		super(params);
		this.state = {
			card: this.props.card,
		};
		this.documentClickListener = () => {
			this.close();
		}
	}
	componentDidMount() {
		document.addEventListener('click', this.documentClickListener);
	}
	componentWillUnmount() {
		document.removeEventListener('click', this.documentClickListener);
	}
	tapAction(e) {
		this.props.card.tap();
		this.close();
	}
	addCounterAction(e, value) {
		this.props.card.addCounter(value);
		this.close();
	}
	resetCounterAction(e) {
		this.props.card.resetCounter();
		this.close();
	}
	moveToLibraryAction(e, front) {
		this.props.card.moveToLibrary(front);
		this.close();
	}
	close(e) {
		this.props.card.closeContextMenu();
	}
	render() {
		return ReactDOM.createPortal(<div className={"card-context-menu"} style={{left:this.props.x + "px",top:this.props.y + "px"}}>
			<a className="item" onClick={(e) => this.tapAction(e)}>Tap / Untap</a>
			<a className="item" onClick={(e) => this.addCounterAction(e, 1)}>Increment Counter</a>
			<a className="item" onClick={(e) => this.addCounterAction(e, -1)}>Decrement Counter</a>
			<a className="item" onClick={(e) => this.resetCounterAction(e)}>Reset Counter</a>
			<a className="item" onClick={(e) => this.moveToLibraryAction(e, true)}>Move to bottom of library</a>
			<a className="item" onClick={(e) => this.moveToLibraryAction(e, false)}>Move to top of library</a>
		</div>, document.body);
	}
}

module.exports = CardContextMenu;
