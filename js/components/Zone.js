
let React = require('react');
let Card = require('./Card');
let DragDrop = require('./../services/DragDrop');

class Zone extends React.Component {
	constructor(params) {
		super(params);
		this.state = {
			cards: this.props.cards,
		};
		this.parent = this.props.parent;
	}
	componentDidMount() {

	}
	componentWillReceiveProps(nextProps) {
		this.setState({
			cards: nextProps.cards,
		});
	}
	getCardStyle(i) {
		return {
			width: (98 / this.state.cards.length()) + "%",
			margin: (1 / this.state.cards.length()) + "%",
		}
	}
	onDrop(e) {
		e.preventDefault();
		var origin = DragDrop.getDragOrigin();
		if (origin.props.name == this.props.name) {
			return;
		}
		var card = DragDrop.getDragData();
		this.parent.moveCard(card, origin.props.name, this.props.name);
		DragDrop.hidePreview();
	}
	onDragOver(e) {
		var ev = e.nativeEvent;
		e.preventDefault();
		var x = ev.pageX - document.body.scrollLeft;
		var y = ev.pageY - document.body.scrollTop;
		DragDrop.setPreviewPosition(x,y);
		ev.dataTransfer.dropEffect = "move";
	}
	removeCard(card) {
		var index = this.state.cards.indexOf(card);
		this.state.cards.splice(index,1);
		this.setState({
			cards: this.state.cards,
		});
	}
	updateCard(card) {
		this.setState({
			cards: this.state.cards.updateCard(card),
		})
	}
	moveCardToZone(card, zoneName, front) {
		this.parent.moveCard(card, this.props.name, zoneName, front);
	}
	render() {
		return <div className={"zone zone-" + this.props.name} style={{width:this.props.size + "%"}}
			onDrop={(e) => this.onDrop(e)} onDragOver={(e) => this.onDragOver(e)}>
			<div className="zone-inner">
			<span className="zone-label">{this.props.name}</span>
			{this.state.cards.toJS().map((card, i) => {
				return <Card zone={this} style={this.getCardStyle(i)} key={i} card={card}/>
			})}
			</div>
		</div>
	}
}

module.exports = Zone;
