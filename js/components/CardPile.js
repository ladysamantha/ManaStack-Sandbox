
let React = require('react');
let Card = require('./Card');
let Zone = require('./Zone');
let debounce = require('debounce');

class CardPile extends React.Component  {
	constructor(params) {
		super();
		this.state = {
			offset: 0,
			numShown: 2,
		};
		this.scrollPile = debounce(this.scrollPile, 10);
	}
	onWheel(e) {
		console.log(e.nativeEvent);
		e.preventDefault();
		this.scrollPile(e.nativeEvent.deltaY > 0 ? 1 : -1);
	}
	scrollPile(up) {
		var offset = this.state.offset + (((up > 0) ? 1 : -1));
		if (offset < 0) offset = 0;
		if (offset > this.props.cards.length() - this.state.numShown) offset = this.props.cards.length() - this.state.numShown;
		this.setState({
			offset: offset
		});
	}
	render() {
		var offset = this.state.offset;
		var cards = this.props.cards.toJS().reverse().slice(offset, offset + this.state.numShown);
		return <div className="card-pile" onWheel={(e) => this.onWheel(e)}>
		{(this.state.offset !== 0 && this.props.cards.length() > this.state.numShown)
		? <div className="pile-button-left" onClick={() => this.scrollPile(-1)}></div>
		: null}
		{cards.map((card, i) => {
			return <Card key={i} card={card} zone={this.props.zone}/>
		})}
		{(this.state.offset !== this.props.cards.length() - this.state.numShown && this.props.cards.length() > this.state.numShown)
			? <div className="pile-button-right" onClick={() => this.scrollPile(1)}></div>
			: null}

		</div>
	}
}

module.exports = CardPile;
