let React = require('react');
let ReactDOM = require('react-dom');

import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';

let Sandbox = require('./components/Sandbox');

let sampleDeck = require('./../deck.json');

ReactDOM.render(<div>
	<Sandbox deck={sampleDeck}></Sandbox>
</div>, document.getElementById('react-app'))
