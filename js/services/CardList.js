let Immutable = require('immutable');

class CardList {
	constructor(cards) {
		if (cards && cards.length !== 0) {
			this.cards = new Immutable.List(cards);
		}
		else {
			this.cards = new Immutable.List();
		}
	}
	updateCard(card) {
		var index = this.cards.indexOf(card);
		return new CardList(this.cards.set(index, card));
	}
	removeCard(card) {
		var index = this.cards.indexOf(card);
		return new CardList(this.cards.remove(index, card));
	}
	addCardToBack(card) {
		return new CardList(this.cards.push(card));
	}
	addCardToFront(card) {
		return new CardList(this.cards.unshift(card));
	}
	slice(index) {
		return this.cards.slice(index);
	}
	splice(index, offset) {
		return this.cards.splice(index, offset);
	}
	toJS() {
		return this.cards.toJS();
	}
	get(index) {
		return this.cards.get(index);
	}
	set(index, card) {
		return this.cards.set(index, card);
	}
	length() {
		return this.cards.size;
	}
	shuffle() {
		var shuffle = function(a) {
			for (let i = a.length - 1; i > 0; i--) {
				const j = Math.floor(Math.random() * (i + 1));
				[a[i], a[j]] = [a[j], a[i]];
			}
			return a;
		}
		return new CardList(shuffle(this.cards.toJS()));
	}
}

module.exports = CardList;
