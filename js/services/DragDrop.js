
class DragDrop {
	constructor() {
		this.dragData = null;
		this.dragOrigin = null;
		this.dragItem = null;
		this.dragging = false;
		this.dragPreview = document.createElement("img");
		this.dragPreview.id = "sandbox-drag-preview";
		document.body.appendChild(this.dragPreview);
	}
	setDragData(data) {
		this.dragData = data;
	}
	getDragData(data) {
		return this.dragData;
	}
	setDragOrigin(origin) {
		this.dragOrigin = origin
	}
	setDragItem(item) {
		 this.dragItem = item;
	}
	getDragOrigin() {
		return this.dragOrigin;
	}
	getDragItem() {
		return this.dragItem;
	}
	setPreview(el) {
		this.dragPreview = el;
	}
	showPreview() {
		if (this.dragPreview) 
			this.dragPreview.classList.add("active");
	}
	hidePreview() {
		if (this.dragPreview) 
			this.dragPreview.classList.remove("active");
	}
	setPreviewImage(src) {
		this.dragPreview.src = src;
	}
	setPreviewPosition(x,y) {
		this.dragPreview.style.left = x + "px";
		this.dragPreview.style.top = y + "px";
	}
}

module.exports = new DragDrop();